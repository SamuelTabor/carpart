# BUT 3ᵉ année - S5 - R5.09
## Virtualisation avancée - TP Carpart

### Liens utiles
- Lien [Gitlab](https://gitlab.com/SamuelTabor/carpart) du projet
- Lien du docker hub de l'image [userAPI](https://hub.docker.com/repository/docker/samueltabor/user-api)
- Lien du docker hub de l'image [productAPI](https://hub.docker.com/repository/docker/samueltabor/product-api)
- Lien du docker hub de l'image [user-db](https://hub.docker.com/repository/docker/samueltabor/user-db)
- Lien du docker hub de l'image [product-db](https://hub.docker.com/repository/docker/samueltabor/product-db)

### Installation:
- Cloner le projet :
```bash
git clone https://gitlab.com/SamuelTabor/carpart.git
```

### Utilisation:
- Lancer le docker-compose :
    ```bash
    docker compose up -d
    ```

  - L'API [**productApi**](http://localhost:8000) est disponible à l'adresse `http://localhost:8000`
  - L'API [**userApi**](http://localhost:8001) est disponible à l'adresse `http://localhost:8001`  

- Arreter la stack docker :
    ```bash
    docker compose down
    ```