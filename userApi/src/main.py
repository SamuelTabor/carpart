import os
import mysql.connector
from fastapi import FastAPI, Response, status
from fastapi.responses import RedirectResponse
from pydantic import BaseModel

# Constantes
MYSQL_DB = os.getenv('MYSQL_INITDB_DATABASE', 'CARPARTS')
MYSQL_HOST = os.getenv('MYSQL_HOST', 'localhost')
MYSQL_PORT = 3306
MYSQL_USER = os.getenv('MYSQL_USER', 'root')
MYSQL_PASSWORD = os.getenv('MYSQL_ROOT_PASSWORD', 'cartPartAdmin')


# Définition du modèle Pydantic
class User(BaseModel):
    nom: str
    mail: str
    nbCommande: int


# Classe pour la gestion de la connexion
class Database:
    def __enter__(self):
        self.cnx = mysql.connector.connect(
            host=MYSQL_HOST,
            port=MYSQL_PORT,
            user=MYSQL_USER,
            password=MYSQL_PASSWORD,
            database=MYSQL_DB
        )
        return self.cnx

    def __exit__(self, exc_type, exc_value, traceback):
        self.cnx.close()


# Connexion à la base de données


# FastAPI definition
app = FastAPI()


@app.get("/", include_in_schema=False)
def root():
    print(f'OS Mongodb_host: {MYSQL_HOST}')
    return RedirectResponse(url="/docs")


@app.post("/users", status_code=201)
async def addUser(user: User, response: Response):
    with Database() as cnx:
        cursor = cnx.cursor()
        # Prepare query
        query = "INSERT INTO Users (nom, mail, nbCommande) VALUES (%s, %s, %s)"
        # Execute query
        cursor.execute(query, (user.nom, user.mail, user.nbCommande))
        # Commit the transaction
        cnx.commit()
        # Check results
        if cursor.rowcount:
            return {"message": "User added", "data": user}
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": "User not added", "data": user}


@app.delete("/users/{userId}", status_code=200)
async def deleteUser(userId: int, response: Response):
    with Database() as cnx:
        # Get cursor
        cursor = cnx.cursor()
        # Prepare query
        query = "DELETE FROM Users WHERE id = %s"
        # Execute query
        cursor.execute(query, (userId,))
        # Commit the transaction
        cnx.commit()
        # Check results
        if cursor.rowcount:
            return {"message": "User deleted", "data": userId}
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "User not found", "data": userId}


@app.put("/users", status_code=204)
async def updateUser(userId: int, user: User, response: Response):
    with Database() as cnx:
        cursor = cnx.cursor()
        # Prepare query
        query = "UPDATE Users SET nom = %s, mail = %s, nbCommande = %s WHERE id = %s LIMIT 1"
        # Execute query
        cursor.execute(query, (user.nom, user.mail, user.nbCommande, userId))
        # Commit the transaction
        cnx.commit()
        # Check results
        if cursor.rowcount:
            return {"message": "User updated", "data": user}
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "User not found", "data": userId}


@app.get("/users/{userId}", status_code=200)
async def getUser(userId: int, response: Response):
    with Database() as cnx:
        cursor = cnx.cursor()
        # Prepare querry
        query = "SELECT * FROM Users where id = %s ;"
        # Execute querry
        cursor.execute(query, (userId,))
        # Get result
        result = cursor.fetchall()
        # Check results
        if result:
            return {"message": "Users found", "data": result}
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "User not found", "data": userId}


@app.get("/users", status_code=200)
async def getUser(response: Response):
    with Database() as cnx:
        cursor = cnx.cursor()
        # Prepare querry
        query = "SELECT * FROM Users;"
        # Execute querry
        cursor.execute(query)
        # Get result
        result = cursor.fetchall()
        # Check results
        if result:
            return {"message": "Users found", "data": result}
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Users not found", "data": None}
