CREATE DATABASE if not exists CARPARTS;
USE CARPARTS;

CREATE TABLE if not exists Users
(
    id         INT AUTO_INCREMENT PRIMARY KEY,
    nom        VARCHAR(255) NOT NULL,
    mail       VARCHAR(255) NOT NULL,
    nbCommande INT          NOT NULL
);

/*
CREATE TABLE if not exists Commandes
(
    id      INT AUTO_INCREMENT PRIMARY KEY,
    userId  INT    NOT NULL,
    itemId  BIGINT NOT NULL,
    nbItems INT    NOT NULL,

    CONSTRAINT FK_userId_User FOREIGN KEY (userId) REFERENCES Users (id)
);
*/