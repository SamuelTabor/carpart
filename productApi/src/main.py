import os
from fastapi import FastAPI, Response, status
from fastapi.responses import RedirectResponse
from pydantic import BaseModel
from pymongo import MongoClient

# Constantes
MONGO_DB = os.getenv('MONGO_INITDB_DATABASE', 'stocks')
MONGO_COLLECTION = 'produits'
MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
MONGO_PORT = 27017


# Définition du modèle Pydantic
class Product(BaseModel):
    productId: int
    productName: str
    porductDescription: str
    stockQuantity: int


def convert_objectid(document):
    if document is not None and "_id" in document:
        document["_id"] = str(document["_id"])
    return document


def get_client():
    # Connexion à la base de données
    return MongoClient(host=MONGO_HOST, port=MONGO_PORT)


# FastAPI definition
app = FastAPI()


@app.get("/", include_in_schema=False)
def root():
    print(f'OS Mongodb_host: {MONGO_HOST}')
    return RedirectResponse(url="/docs")


@app.post("/add", status_code=201)
def addProduit(product: Product, response: Response):
    # Init client connection to a collection
    collection = get_client()[MONGO_DB][MONGO_COLLECTION]

    # Check if the id of the product is already in the database
    existing_product = convert_objectid(collection.find_one({"productId": product.productId}))
    if existing_product is not None:
        existing_product = Product.parse_obj(existing_product)
        response.status_code = status.HTTP_409_CONFLICT
        return {"message": "Product with same id already exists", "data": existing_product}
    else:
        # Insert the product in the database
        result = collection.insert_one(product.dict())
        print(f'Inserted document id: {result.inserted_id}')
        return {"message": "Product added", "data": product}


@app.delete("/remove", status_code=200)
def removeProduit(productId: int, response: Response):
    # Init client connection to a collection
    collection = get_client()[MONGO_DB][MONGO_COLLECTION]

    # Check if the id of the product is already in the database
    existing_product = convert_objectid(collection.find_one({"productId": productId}))
    if existing_product is not None:
        # Delete the product in the database
        existing_product = Product.parse_obj(existing_product)
        result = collection.delete_one({"productId": productId})
        print(f'Deleted document id: {result.deleted_count}')
        return {"message": "Product deleted", "data": existing_product}
    # If no product found
    response.status_code = status.HTTP_404_NOT_FOUND
    return {"message": "No product matching with the id", "data":  {"productId": productId}}



@app.put("/update", status_code=201)
def updateProduit(product: Product, response: Response):
    # Init client connection to a collection
    collection = get_client()[MONGO_DB][MONGO_COLLECTION]

    # Check if the id of the product is in the database
    existing_product = convert_objectid(collection.find_one({"productId": product.productId}))
    if existing_product is not None:
        existing_product = Product.parse_obj(existing_product)
        # Update the product in the database
        result = collection.update_one({"productId": product.productId}, {"$set": product.dict()})
        print(f'Updated document id: {result.upserted_id}')
        return {"message": "Product updated", "data": {"before": existing_product, "after": product}}

    response.status_code = status.HTTP_404_NOT_FOUND
    return {"message": "Product not found, not updated"}


@app.get("/produit/{productId}")
def getProduit(productId: int, response: Response):
    # Init client connection to a collection
    collection = get_client()[MONGO_DB][MONGO_COLLECTION]

    # Check if the id of the product is in the database
    existing_product = convert_objectid(collection.find_one({"productId": productId}))
    if existing_product is not None:
        existing_product = Product.parse_obj(existing_product)
        return {"message": "Product found", "data": existing_product}
    response.status_code = status.HTTP_404_NOT_FOUND
    return {"message": "Product not found", "data": productId}

@app.get("/products")
def getAll():
    collection = get_client()[MONGO_DB][MONGO_COLLECTION]
    # Get all products from the database
    products = list(collection.find())

    # Convert ObjectIds to strings
    for product in products:
        convert_objectid(product)

    return {"message": "Products retrieved", "data": products}