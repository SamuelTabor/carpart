const databaseName = process.env.MONGO_INITDB_DATABASE;
db = db.getSiblingDB(databaseName); // sélectionne la base de données spécifiée dans l'environnement docker'

// Création de la collection 'produits' avec le schéma
db.createCollection('produits', {
    validator: {
        $jsonSchema: {
            bsonType: "object",
            required: ["productId", "stockQuantity"],
            properties: {
                productId: {
                    bsonType: "int",
                    description: "must be an integer and is required"
                },
                productName: {
                    bsonType: "string",
                    description: "must be a string if the field exists"
                },
                productDescription: {
                    bsonType: "string",
                    description: "must be a string if the field exists"
                },
                stockQuantity: {
                    bsonType: "int",
                    description: "must be an integer and is required"
                }
            }
        }
    }
});